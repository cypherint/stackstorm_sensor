from st2reactor.sensor.base import PollingSensor


class SampleSensorFail(PollingSensor):
    """
    * self.sensor_service
        - provides utilities like
            - get_logger() - returns logger instance specific to this sensor.
            - dispatch() for dispatching triggers into the system.
    * self._config
        - contains parsed configuration that was specified as
          config.yaml in the pack.
    """
    def __init__(self, sensor_service, config, poll_interval):
        super(SampleSensor, self).__init__(
                sensor_service=sensor_service,
                config=config,
                poll_interval=poll_interval
            )

    def setup(self):
        # Setup stuff goes here. For example, you might establish connections
        # to external system once and reuse it. This is called only once by the system.
        self.logger = self.sensor_service.get_logger(name=self.__class__.__name__)

    def cleanup(self):
        # This is called when the st2 system goes down. You can perform cleanup operations like
        # closing the connections to external system here.
        pass

    def poll(self):
        self.logger.info("sample sensor run")
        trigger = "sensorsample.samplesensorfail"
        payload = {"sampledata":"samplevalue"}
        raise RuntimeError


    def add_trigger(self, trigger):
        # This method is called when trigger is created
        pass

    def update_trigger(self, trigger):
        # This method is called when trigger is updated
        pass

    def remove_trigger(self, trigger):
        # This method is called when trigger is deleted
        pass
